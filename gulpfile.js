const {src, dest, series, watch} = require('gulp')
const sass = require('gulp-sass')(require('sass'))
const csso = require('gulp-csso')
const include = require('gulp-file-include')
const htmlmin = require('gulp-htmlmin')
const del = require('del')
const concat = require('gulp-concat')
const autoprefixer = require('gulp-autoprefixer')
const sync = require('browser-sync').create()

function html() {
    return src('src/html/**')
        .pipe(include({
            prefix: '@@'
        }))
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(dest('dist'))
}

const scss = () => {
    return src('src/scss/style.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            overrideBrowserslist: [
                '>1%',
            ],
        }))
        .pipe(csso())
        .pipe(concat('style.css'))
        .pipe(dest('dist/css'));
}

const script = () => {
    return src('src/js/**')
        .pipe(dest('dist/js'));
}

function clear() {
    return del('dist')
}

function serve() {
    sync.init({
        server: './dist'
    })

    watch('src/html/**.html', html).on('change', sync.reload)
    watch('src/scss/**/**.scss', scss).on('change', sync.reload)
    watch('src/js/**', script).on('change', sync.reload);
}

exports.clear = clear;
exports.build = series(clear, scss, script, html);
exports.default = series(clear, scss, script, html, serve);
