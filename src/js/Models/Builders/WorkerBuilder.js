import Worker from '../Enities/Worker.js';

class WorkerBuilder {

  buildWorker(data) {
    const worker = new Worker();
    worker.id = data.get('id');
    worker.name = data.get('name');
    worker.surname = data.get('surname');
    worker.position = data.get('position_id');
    worker.birthday = data.get('birthday');
    worker.head = data.get('head');
    worker.company = data.get('company');
    worker.login = data.get('login');
    worker.password = data.get('password');

    return worker;
  }
}

export default WorkerBuilder;