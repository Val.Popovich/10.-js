class Worker {
  constructor() {

    this._id = null;
    this._name = null;
    this._surname = null;
    this._position_id = null;
    this._birthday = null;
    this._head_id = null;
    this._company_id = null;
    this._login = null;
    this._password = null;
  }

  set id(id) {
    this._id = id;
  }

  set name(name) {
    this._name = name;
  }

  set surname(surname) {
    this._surname = surname;
  }

  set position(position_id) {
    this._position_id = position_id;
  }

  set birthday(birthday) {
    this._birthday = birthday;
  }

  set head(head_id) {
    this._head_id = head_id;
  }

  set company(company_id) {
    this._company_id = company_id;
  }

  set login(login) {
    this._login = login;
  }

  set password(password) {
    this._password = password;
  }

  get id() {
    return this._id;
  }

  get name() {
    return this._name;
  }

  get surname() {
    return this._surname;
  }

  get position() {
    return this._position_id;
  }

  get birthday() {
    return this._birthday;
  }

  get head() {
    return this._head_id;
  }

  get company() {
    return this._company_id;
  }

  get login() {
    return this._login;
  }

  get password() {
    return this._password;
  }
}

export default Worker;
