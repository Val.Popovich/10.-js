import WorkerBuilder from '../Models/Builders/WorkerBuilder.js';
import Router from '../Routers/Router.js';
import ApiModel from '../Api/ApiModel.js';

class HomeView {
  constructor() {
    this._section = document.querySelector('.js-section');
    this.apiModel = new ApiModel('worker');
  }

  render() {
    this._section.innerHTML = '';

    this._section.insertAdjacentHTML('beforeend', `
    <form action="/worker" class="js-add-worker form">
        <h1 class="form__head">Добавление работника:</h1>
        <div class="form__field">
            <label for="worker_name" class="form__label">Имя:</label>
            <input
                    type="text"
                    value="kek"
                    id="worker_name"
                    class="form__input"
                    name="name"
            >
        </div>
        <div class="form__field">
            <label for="worker_surname" class="form__label">Фамилия:</label>
            <input
                    type="text"
                    value="kek"
                    id="worker_surname"
                    class="form__input"
                    name="surname"
            >
        </div>
        <div class="form__field">
            <label for="worker_position_id" class="form__label">Должность:</label>
            <input
                    type="text"
                    value="1"
                    id="worker_position_id"
                    class="form__input"
                    name="position_id"
            >
        </div>
        <div class="form__field">
            <label for="worker_birthday" class="form__label">Дата рождения:</label>
            <input
                    type="text"
                    value="2020-03-02"
                    id="worker_birthday"
                    class="form__input"
                    name="birthday"
            >
        </div>
        <div class="form__field">
            <label for="worker_head" class="form__label">Начальник</label>
            <input
                    type="text"
                    value="3"
                    id="worker_head"
                    class="form__input"
                    name="head"
            >
        </div>
        <div class="form__field">
            <label for="worker_company" class="form__label">Компания</label>
            <input
                    type="text"
                    value="2"
                    id="worker_company"
                    class="form__input"
                    name="company"
            >
        </div>
        <div class="form__field">
            <label for="worker_login" class="form__label">Логин:</label>
            <input
                    type="text"
                    value="hjkofghjkshhshh"
                    id="worker_login"
                    class="form__input"
                    name="login"
            >
        </div>
        <div class="form__field">
            <label for="worker_password" class="form__label">Пароль:</label>
            <input
                    type="password"
                    value="ertyuio"
                    id="worker_password"
                    class="form__input"
                    name="password"
            >
        </div>
        <button class="js-form__btn form__btn btn" type="submit">Сохранить</button>
    </form>
    `);

    this.initAddWorkerFormListener();
  }

  initAddWorkerFormListener() {
    const form = document.querySelector('.js-add-worker');

    if (!form) {
      return;
    }

    form.addEventListener('submit', async (e) => {
      e.preventDefault();

      const worker = new WorkerBuilder().buildWorker(new FormData(form));

      await this.apiModel.updateWorker('POST', worker);

      const router = new Router();
      router.route('/workers').render();
    });
  }
}

export default HomeView;
