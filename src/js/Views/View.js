import Router from '../Routers/Router.js';
import WorkersView from './WorkersView.js';

class View {
  static initMenuListener() {
    const menu = document.querySelector('.menu');

    menu.addEventListener('click', (e) => {
      if (!e.target.closest('.header__link')) {
        return;
      }

      e.preventDefault();

      const router = new Router();
      router.route(e.target.getAttribute('href')).render();
    });
  }

  static initAddWorkerListListener() {
    const section = document.querySelector('.js-section');

    section.addEventListener('click', (e) => {
      const itemLink = e.target.closest('.js-list-item__link');

      if (itemLink) {
        e.preventDefault();
        (new WorkersView).getWorkerInfo(itemLink);
      }
    });
  }
}

export default View;