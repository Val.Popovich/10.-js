import ApiModel from '../Api/ApiModel.js';
import Router from '../Routers/Router.js';

const ID_PART_OF_URL = 2;

  class WorkersView {
  constructor() {
    this.section = document.querySelector('.js-section');
    this.instanceApi = new ApiModel('worker');
  }

  async render() {
    this.section.innerHTML = '';
    const template = Handlebars.compile(document.querySelector('#workerCard').innerHTML);
    const workers = await this.instanceApi.getWorkers();

    workers.map(worker =>
      this.section.insertAdjacentHTML('beforeend', template({'worker': worker}))
    );
  }

  getWorkerInfo(itemLink) {
    const linkPath = itemLink.getAttribute('href');

    const router = new Router();
    const workerView = router.route(linkPath);

    const workerId = linkPath.split('/')[ID_PART_OF_URL];

    workerView.workerId = workerId;
    workerView.render();
  }
}

export default WorkersView;
