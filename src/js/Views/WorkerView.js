import ApiModel from '../Api/ApiModel.js';
import WorkerBuilder from '../Models/Builders/WorkerBuilder.js';
import Router from '../Routers/Router.js';

class WorkerView {
  constructor() {
    this._workerId = null;
    this.apiModel = new ApiModel('worker');
    this._section = document.querySelector('.js-section');
  }

  set workerId(id) {
    this._workerId = id;
  }

  get workerId() {
    return this._workerId;
  }

  async render() {
    this._section.innerHTML = '';

    const workerTemplate = Handlebars.compile(document.querySelector('#worker').innerHTML);
    const updateWorkerTemplate = Handlebars.compile(document.querySelector('#workerUpdateForm').innerHTML);

    const worker = await this.apiModel.getWorkerById(this.workerId);
    this._section.insertAdjacentHTML('beforeend', workerTemplate({worker}))
    this._section.insertAdjacentHTML('beforeend', updateWorkerTemplate({worker}))

    this.initWorkerListeners();
  }

  initWorkerListeners() {
    this.initUpdateWorkerFormListener();
    this.initDeleteWorkerBtnListener();
  }

  initUpdateWorkerFormListener() {
    const updateForm = document.querySelector('.js-update-form');

    updateForm.addEventListener('submit', async (e) => {
      e.preventDefault();

      const worker = new WorkerBuilder().buildWorker(new FormData(updateForm));

      await this.apiModel.updateWorker('PUT', worker);

      const router = new Router();
      router.route('/workers').render();
    });
  }

  initDeleteWorkerBtnListener() {
    const deleteBtn = document.querySelector('.js-delete-btn');

    deleteBtn.addEventListener('click', async (e) => {
      e.preventDefault();

      await this.apiModel.deleteWorker(this.workerId);

      const router = new Router();
      router.route('/workers').render();
    });
  }
}

export default WorkerView;