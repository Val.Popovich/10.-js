import Router from './Routers/Router.js';
import View from './Views/View.js';

const router = new Router();

View.initAddWorkerListListener();

router.route('/').render();
View.initMenuListener();
