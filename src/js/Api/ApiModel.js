class ApiModel {
  constructor(name) {
    this.apiUrl = `http://lera-mvc.local/api/${name}`;
  }

  async getWorkers() {
    try {
      return this.sendRequest();
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async getWorkerById(id) {
    try {
      return this.sendRequest(id);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async deleteWorker(id) {
    await fetch(`${this.apiUrl}s/${id}`, {
      method: 'DELETE',
    });
  }

  async updateWorker(method, worker) {
    await fetch(`${this.apiUrl}s`,{
      method: method,
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(worker),
    });
  }

  async sendRequest(id = null) {
    let data;
    if (id === null) {
      data = await fetch(`${this.apiUrl}s`);
    } else {
      data = await fetch(`${this.apiUrl}s/${id}`);
    }
    return data.json();
  }
}

export default ApiModel;