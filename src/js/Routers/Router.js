import HomeView from '../Views/HomeView.js';
import WorkerView from '../Views/WorkerView.js';
import WorkersView from '../Views/WorkersView.js';

class Router {
  routes = {
    '/'              : new HomeView(),
    '/workers'       : new WorkersView(),
    '/workers/\\d+'  : new WorkerView(),
  }

  route(path) {
    let controller = null;

    for (let key in this.routes) {
      let regex = new RegExp('^' + key + '$');

      if (path.match(regex)) {
        controller = this.routes[key];
        break;
      }
    }

    if (controller) {
      return controller;
    }
  }
}

export default Router;